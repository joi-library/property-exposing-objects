/**
 * This package contains classes that can be used to serialize objects and
 * persist them or other exchanges. This package does not contain any logic or 
 * interfaces to serialize, only interfaces to make serializable objects.
 * 
 * The goal is to replace the default Java Serializable interface and 
 * reflection that comes along with it. 
 */
package com.github.elwinbran.javaoverhaul.peo;
