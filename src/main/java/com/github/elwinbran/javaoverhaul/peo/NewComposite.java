/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.elwinbran.javaoverhaul.peo;

import com.github.elwinbran.javaoverhaul.base.Bytes;
import com.github.elwinbran.javaoverhaul.containers.Table;

/**
 *
 * @author Elwin Slokker
 */
public interface NewComposite
{
    /**
     * 
     * @return 
     */
    public abstract Table<Bytes, PropertyValue> properties();
}
