/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.peo;

import com.github.elwinbran.javaoverhaul.base.Bytes;
import com.github.elwinbran.javaoverhaul.containers.EmptyCheckContainer;
import com.github.elwinbran.javaoverhaul.containers.Table;

/**
 * Produces objects that expose all their properties.
 * Has a full production method, as well as more specialized methods.
 * 
 * @author Elwin Slokker
 * @see ExposingComposite
 */
public interface ExposingCompositeFactory
{
    /**
     * Produces a nested property with only a single primitive value.
     * 
     * @param propertyIdentifier the binary value that identifies the property.
     * @param propertyValue the primitive (binary) value assigned to the
     * sole property.
     * @return a nested property with only a single primitive value
     */
    public abstract ExposingComposite makeFromPrimitive(Bytes propertyIdentifier,
            Bytes propertyValue);
    
    /**
     * Produces a nested property with only a single primitive sequence.
     * 
     * @param propertyIdentifier the binary value that identifies the property.
     * @param propertyValue the primitive (binary) sequence value assigned to
     * the sole property.
     * @return a nested property with only a single primitive sequence
     */
    public abstract ExposingComposite makeFromPrimitiveSequence(
            Bytes propertyIdentifier, EmptyCheckContainer<Bytes> propertyValue);
    
    /**
     * Produces a nested property with only a nested composite.
     * 
     * @param propertyIdentifier the binary value that identifies the property.
     * @param propertyValue the composite value assigned to the sole property.
     * @return a nested property with only a nested composite
     */
    public abstract ExposingComposite makeFromComposite(
            Bytes propertyIdentifier, ExposingComposite propertyValue);
    
    /**
     * Produces a nested property with only a sequence of nested composites.
     * 
     * @param propertyIdentifier the binary value that identifies the property.
     * @param propertyValue the sequence of composites assigned to the sole
     * property.
     * @return a nested property with only a sequence of nested composites
     */
    public abstract ExposingComposite makeFromCompositeSequence(
            Bytes propertyIdentifier, 
            EmptyCheckContainer<ExposingComposite> propertyValue);
    
    /**
     * Produces a nested property object with the exact behaviour as the given
     * arguments.
     * At least one of the arguments must satisfy 
     * {@link Table#pairs()}{@code != 0}.
     * 
     * @param primitiveSinglePairs all primitive properties to appear in the 
     * nested property.
     * @param primitiveSequencePair all primitive sequence properties to appear 
     * in the nested property.
     * @param singleComposedPairs all composed properties to appear in the 
     * nested property.
     * @param composedSequencePairs all composed sequence properties to appear 
     * in the  nested property.
     * @return An instance that has the exact behaviour as provided through this
     * method.
     */
    public abstract ExposingComposite make(
        Table<Bytes, Bytes> primitiveSinglePairs,
        Table<Bytes, EmptyCheckContainer<Bytes>> primitiveSequencePair,
        Table<Bytes, ExposingComposite> singleComposedPairs,
        Table<Bytes, EmptyCheckContainer<ExposingComposite>> composedSequencePairs);
}
