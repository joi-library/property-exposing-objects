/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.peo;

import com.github.elwinbran.javaoverhaul.base.Bytes;
import com.github.elwinbran.javaoverhaul.containers.Table;
import com.github.elwinbran.javaoverhaul.containers.EmptyCheckContainer;

/**
 * A class that can only expose all its 'properties'/'attributes' or components
 * and nothing else. The purpose of this class is to specify a universal format of a 
 * serializable object as the name implies.
 * Every instance should be part of a tree structure (no loops by pointing in 
 * cycles) and the 'leaves' are byte arrays or collections of arrays.
 * Since bytes are almost universally serializable, that makes every instance
 * of this class also almost univerasally serializable.
 * 
 * Properties or attributes are represented by {@link Table tables}, where the 
 * name is the key and the value is real value of the property/attribute.
 * 
 * @author Elwin Slokker
 * @see Table
 * @see Bytes
 */
public interface ExposingComposite
{
    /**
     * The properties that store primitive (universally serializable) values.
     * 
     * @return All primitive properties of this object.
     */
    public abstract Table<Bytes, Bytes> primitiveSinglePairs();
    
    /**
     * The properties that store (un)sorted sequences of primitives.
     * Think of a collection of measurements for example. In this collection it 
     * is impossible to distinguish to measurements from another. 
     * That is the kind of property that will appear in this method.
     * 
     * @return All properties of this object that are sequences of byte arrays.
     */
    public abstract Table<Bytes, EmptyCheckContainer<Bytes>>
        primitiveSequencePairs();
    
    /**
     * The properties that store composites.
     * 
     * @return All properties that store 'composites' better known as objects.
     */
    public abstract Table<Bytes, ExposingComposite> singleComposedPairs();
    
    /**
     * The properties that store (un)sorted sequences of composites.
     * To present a classic example: a collection of students. They are 
     * definitely objects due to having many fields but one would not bother 
     * making a class that has a property for every individual 
     * 
     * @return 
     */
    public abstract Table<Bytes, EmptyCheckContainer<ExposingComposite>>
        composedSequencePairs();
}
