/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.elwinbran.javaoverhaul.peo;

import com.github.elwinbran.javaoverhaul.base.Bytes;
import com.github.elwinbran.javaoverhaul.base.NullableWrapper;

/**
 *
 * @author Elwin Slokker
 */
public class Example
{
    public static void main(String[] args)
    {
        NewComposite newCompo = null;
        ExposingComposite serCompo = null;
    }
    
    private static boolean isValidBub(NewComposite possibleBub)
    {
        //get property id/name from validator
        NullableWrapper<PropertyValue> possibleProperty = 
                possibleBub.properties().lookup(null, null);
        if(!possibleProperty.isNil())
        {//extractable
            PropertyValue possibleBubName = possibleProperty.getWrapped();
            //get what kind it must be from the validator
            NullableWrapper<Bytes> possibleName = possibleBubName.primitive();
            if(!possibleName.isNil())
            {
                Bytes nameValue = possibleName.getWrapped();
            }
        }
        
    }
    
    private static boolean isValidBub(ExposingComposite possibleBub)
    {
        //get property id/name from validator and type
        NullableWrapper<Bytes> possibleName = 
                possibleBub.primitiveSinglePairs().lookup(null, null);
        if(!possibleName.isNil())
        {
            Bytes nameValue = possibleName.getWrapped();
        }
    }
}
